package com.techgroup.girapayv2.streamprocessor;

import com.techgroup.girapayv2.streamprocessor.model.BonusMiningDailyUpdateMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class Producer {

    private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);


    @Value("${kafka.topic.outgoing}")
    private String outgoingTopic;

    @Autowired
    private KafkaTemplate<String, BonusMiningDailyUpdateMsg> kafkaTemplate;


    public void send(BonusMiningDailyUpdateMsg bonusMiningDailyUpdateMsg) {
        LOGGER.info("Sending message='{}'", bonusMiningDailyUpdateMsg.toString());
        kafkaTemplate.send(outgoingTopic, bonusMiningDailyUpdateMsg);
    }
}
