package com.techgroup.girapayv2.streamprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamprocessorApplication {

    public static void main(String[] args) {
        SpringApplication.run(StreamprocessorApplication.class, args);
    }
}
