package com.techgroup.girapayv2.streamprocessor.model;


public class BonusMiningDailyUpdateMsg {

    private Long miningId;
    private String transactionId;

    public Long getMiningId() {

        return miningId;
    }

    public void setMiningId(Long miningId) {
        this.miningId = miningId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "BonusMiningDailyUpdateMsg{" +
                "miningId=" + miningId +
                ", transactionId='" + transactionId + '\'' +
                '}';
    }
}
