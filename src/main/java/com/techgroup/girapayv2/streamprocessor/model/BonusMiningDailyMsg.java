package com.techgroup.girapayv2.streamprocessor.model;

import java.math.BigDecimal;
import java.util.Date;

public class BonusMiningDailyMsg {

    private Long miningId;
    private BigDecimal amount;
    private BigDecimal conversionRate;
    private Date date = new Date();
    private String address;


    public BonusMiningDailyMsg() {
        super();
    }

    public BonusMiningDailyMsg(Long miningId, BigDecimal amount, BigDecimal conversionRate, Date date, String address) {
        super();
        this.miningId = miningId;
        this.amount = amount;
        this.conversionRate = conversionRate;
        this.date = date;
        this.address = address;
    }


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(BigDecimal conversionRate) {
        this.conversionRate = conversionRate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getMiningId() {

        return miningId;
    }

    public void setMiningId(Long miningId) {
        this.miningId = miningId;
    }

    @Override
    public String toString() {
        return "BonusMiningDailyMsg{" +
                "miningId=" + miningId +
                ", amount=" + amount +
                ", conversionRate=" + conversionRate +
                ", date=" + date +
                ", address='" + address + '\'' +
                '}';
    }

}
