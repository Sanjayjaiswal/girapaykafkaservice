package com.techgroup.girapayv2.streamprocessor;

import com.techgroup.girapayv2.streamprocessor.model.BonusMiningDailyMsg;
import com.techgroup.girapayv2.streamprocessor.model.BonusMiningDailyUpdateMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class Receiver {

    private static final Logger LOGGER = LoggerFactory.getLogger(Receiver.class);

    @Autowired
    private Producer producer;

    @KafkaListener(topics = "${kafka.topic.incoming}")
    public void receive(BonusMiningDailyMsg bonusMiningDailyMsg) {
        LOGGER.info("Received message='{}'", bonusMiningDailyMsg.toString());

        // Do some processing
        // And send the result to the other topic
        BonusMiningDailyUpdateMsg bonusMiningDailyUpdateMsg = new BonusMiningDailyUpdateMsg();
        bonusMiningDailyUpdateMsg.setMiningId(bonusMiningDailyMsg.getMiningId());
        String transactionId = UUID.randomUUID().toString();
        bonusMiningDailyUpdateMsg.setTransactionId(transactionId);

        
        producer.send(bonusMiningDailyUpdateMsg);

    }
}
